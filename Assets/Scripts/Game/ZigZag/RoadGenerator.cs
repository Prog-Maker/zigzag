﻿using Game.Common;
using UnityEngine;

namespace Game.ZigZag
{
    public class RoadGenerator : EventListernerMonoBehaviour
    {
        [SerializeField]
        private GameObjectPool[] pools;

        [SerializeField]
        private int currentPoolIndex = 0;

        [SerializeField]
        private int countPlatformsOnStart = 20;

        [SerializeField]
        private Vector3 startPos;

        [SerializeField]
        private CounterType counterType;

        private float offset = 2;

        private int counter = 0, deathTileCounter = 0;

        private Size size;

        private void Start()
        {
            Init();
        }

        private void Init()
        {
            var prefab = pools[currentPoolIndex].GetPrefab;

            size = prefab.GetComponent<Size>();

            offset = size.GetSize;

            startPos = size.StartPosition;

            pools[currentPoolIndex].gameObject.SetActive(true);

            for (int i = 0; i < countPlatformsOnStart; i++)
            {
                SetNextPlatforms();
            }
        }

        private void SetNextLevel()
        {
            currentPoolIndex++;

            if (currentPoolIndex >= pools.Length)
            {
                currentPoolIndex = 0;
            }
        }

        private void SetNextPlatforms()
        {
            for (int j = 0; j < 5; j++)
            {
                GameObject tile;

                if (IsForward)
                {
                    tile = SetTile(new Vector3(0, 0, offset));
                }
                else
                {
                    tile = SetTile(new Vector3(offset, 0, 0));
                }

                ShowCristal(j, tile);
            }

            UpdateCounter();
        }

        private void UpdateCounter()
        {
            switch (counterType)
            {
                case CounterType.Random:
                    counter = Random.Range(0, 6);
                    break;
                case CounterType.Сonsistently:
                    counter++;

                    if (counter > 5)
                    {
                        counter = 0;
                    }
                    break;
            }
        }

        private void ShowCristal(int index, GameObject tile)
        {
            if (counter == index)
            {
                tile.GetComponent<ObjectActivator>()?.Activate();
            }
        }


        private GameObject SetTile(Vector3 offset)
        {
            var pos = startPos;
            pos += offset;
            var tile = InstatiatePlatfrom(pos);
            startPos = pos;

            return tile;
        }

        private GameObject InstatiatePlatfrom(Vector3 pos)
        {
            var go = pools[currentPoolIndex].GetFromPool();

            go.SetActive(true);

            go.transform.position = pos;

            return go;
        }

        private bool IsForward
        {
            get
            {
                if (Random.Range(0, 2) == 1)
                {
                    return false;
                }
                return true;
            }
        }

        public override void OnMMEvent(MMGameEvent eventType)
        {
            if (eventType.GameEventType == GameEvents.RestartLevel)
            {
                pools[currentPoolIndex].DeactivateAll();

                pools[currentPoolIndex].gameObject.SetActive(false);
                
                SetNextLevel();

                Init();
            }

            if (eventType.GameEventType == GameEvents.StateComplete)
            {
                deathTileCounter++;

                if (deathTileCounter == 5)
                {
                    SetNextPlatforms();
                    deathTileCounter = 0;
                }
            }
        }

    }

    public enum CounterType
    {
        Random, Сonsistently
    }
}
