﻿using Game.Common;
using UnityEngine;

namespace Game.ZigZag
{
    public class StartManager : EventListernerMonoBehaviour
    {
        private bool gameIsStarting = false;

        private void Update()
        {
            if (!gameIsStarting)
            {
                if (Click())
                {
                    MMGameEvent.Trigger(GameEvents.StartGame);
                    gameIsStarting = true;
                }
            }
        }


        protected virtual bool Click()
        {
            if (Input.GetMouseButtonDown(0))
            {
                return true;
            }

            return false;
        }

        public override void OnMMEvent(MMGameEvent eventType)
        {
            if(eventType.GameEventType == GameEvents.RestartLevel)
            {
                gameIsStarting = false;
            }
        }
    }
}
