﻿using Game.Common;
using UnityEngine;

namespace Game.ZigZag
{
    public class InputManager : MonoBehaviour
    {
        void Update()
        {
            Click();
        }

        protected virtual void Click()
        {
            if (Input.GetMouseButtonDown(0))
            {
                MMGameEvent.Trigger(GameEvents.OnClick);
            }
        }
    }
}
