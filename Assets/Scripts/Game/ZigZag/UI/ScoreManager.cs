﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Game.Common
{
    public class ScoreManager : EventListernerMonoBehaviour
    {
        [SerializeField]
        private Text scoreTextBox;
        
        [SerializeField]
        private string scoreName = "Cristal";

        private double scoreCount = 0;

        private void Start()
        {
            UpdateText();
        }


        public override void OnMMEvent(MMGameEvent eventType)
        {
            if (eventType.GameEventType == GameEvents.AddScore && eventType.EventName == scoreName)
            {
                scoreCount += Convert.ToDouble(eventType.Arg);
                UpdateText();
            }
        }

        private void UpdateText()
        {
            scoreTextBox.text = scoreCount.ToString();
        }
    }
}
