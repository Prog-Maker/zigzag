﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Game.ZigZag
{
    public class Size : MonoBehaviour
    {
        [SerializeField]
        private Transform childModel;

        [SerializeField]
        private float size;

        [SerializeField]
        private Vector3 startPos;
        public float GetSize => size;
        public Vector3 StartPosition => startPos;
    }
}
