﻿using Game.Common;
using UnityEngine;

namespace Game.ZigZag
{
    public class PlayerController : EventListernerMonoBehaviour
    {
        [SerializeField]
        private float speed = 3;

        [SerializeField]
        private Vector3 startPoint = new Vector3(0, 1, 0);

        private float x = 0, z = 1;

        private bool canMove = false;
        private bool isForward = true;

        void Update()
        {

            if (canMove)
            {
                transform.Translate(x * speed * Time.deltaTime, 
                                    0, 
                                    z * speed * Time.deltaTime, 
                                    Space.World);
            }

            Die();
        }

        public override void OnMMEvent(MMGameEvent eventType)
        {
            switch (eventType.GameEventType)
            {
                case GameEvents.StartGame:
                    canMove = true;
                    break;
                case GameEvents.OnClick:
                    SetDirection();
                    break;
   
            }
        }

        private void SetDirection()
        {
            if (isForward)
            {
                x = 1;
                z = 0;
                isForward = false;
            }
            else
            {
                x = 0;
                z = 1;
                isForward = true;
            }
        }

        private void Die()
        {
            if (transform.position.y < -5)
            {
                Restart();
            }
        }

        private void Restart()
        {
            transform.position = startPoint;
            transform.rotation = Quaternion.Euler(Vector3.zero);
            canMove = false;
            MMGameEvent.Trigger(GameEvents.RestartLevel);
        }
    }
}
