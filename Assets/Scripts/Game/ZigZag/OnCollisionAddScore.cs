﻿using Game.Common;
using UnityEngine;

namespace Game.ZigZag
{
    public class OnCollisionAddScore : MonoBehaviour
    {
        [SerializeField]
        private string scoreName;

        [SerializeField]
        private double scoreCount;

        private void OnTriggerEnter(Collider other)
        {
            MMGameEvent.Trigger(GameEvents.AddScore, scoreName, scoreCount);
            gameObject.SetActive(false);
        }
    } 
}
