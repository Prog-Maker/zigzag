﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Game.Common {
    public class EventListernerMonoBehaviour : MonoBehaviour, MMEventListener<MMGameEvent>
    {
        private void OnEnable()
        {
            this.MMEventStartListening();
        }

        private void OnDisable()
        {
            this.MMEventStopListening();
        }

        public virtual void OnMMEvent(MMGameEvent eventType)
        { }
    } 
}
