﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Game.Common
{
    public interface IPool<T>
    {
        void CreatePool();
        T GetFromPool();
    }
}
