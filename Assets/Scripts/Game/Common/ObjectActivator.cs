﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Game.Common
{
    public class ObjectActivator : MonoBehaviour
    {
        [SerializeField]
        private GameObject objectToActivate;

        public void Activate()
        {
            objectToActivate.SetActive(true);
        }
    }
}
