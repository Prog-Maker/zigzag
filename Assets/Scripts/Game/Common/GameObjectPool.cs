﻿using UnityEngine;

namespace Game.Common
{
    public class GameObjectPool : MonoBehaviour, IPool<GameObject>
    {
        [SerializeField]
        private GameObject prefabToPooling;

        [SerializeField]
        private int poolSize;

        [SerializeField]
        private bool canExpand = true;

        public GameObject GetPrefab => prefabToPooling;

        private void Start()
        {
            CreatePool();
        }

        public void CreatePool()
        {
            for (int i = 0; i < poolSize; i++)
            {
                var obj = Instantiate(prefabToPooling);
                obj.transform.SetParent(transform);
                obj.gameObject.SetActive(false);
            }
        }

        public GameObject GetFromPool()
        {
            GameObject obj;

            for (int i = 0; i < transform.childCount; i++)
            {
                var go = transform.GetChild(i).gameObject;

                if (!go.gameObject.activeSelf)
                {
                    return go;
                }
            }

            if (canExpand)
            {
                obj = Instantiate(prefabToPooling);
                obj.transform.SetParent(transform);
                return obj;
            }

            return null;
        }

        public void DeactivateAll()
        {
            for (int i = 0; i < transform.childCount; i++)
            {
                var go = transform.GetChild(i).gameObject;

                if (go.activeSelf)
                {
                    go.SetActive(false);
                }
            }
        }
    }
}
