﻿using UnityEngine;

namespace Game.Common
{
    public class BasePool<T> : MonoBehaviour, IPool<T> where T : Component
    {
        [SerializeField]
        private T prefabToPooling;

        [SerializeField]
        private int poolSize;

        [SerializeField]
        private bool canExpand = true;

        private void Start()
        {
            CreatePool();
        }

        public void CreatePool()
        {
            for (int i = 0; i < poolSize; i++)
            {
                var obj = Instantiate(prefabToPooling);
                obj.transform.SetParent(transform);
                obj.gameObject.SetActive(false);
            }
        }

        public T GetFromPool()
        {
            T obj;

            for (int i = 0; i < transform.childCount; i++)
            {
                var go = transform.GetChild(i).gameObject;

                if (!go.gameObject.activeSelf)
                {
                    obj = go.GetComponent<T>();
                    return obj;
                }
            }

            if (canExpand)
            {
                obj = Instantiate(prefabToPooling);
                obj.transform.SetParent(transform);
                return obj;
            }

            return null;
        }
    }
}
