﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Game.Common
{
    public enum GameEvents
    {
        StartGame,
        RestartLevel,
        StateComplete, 
        OnClick, 
        Counter, 
        SetScore, 
        GetScore, 
        AddScore, 
        UpdateScore, 
        LocalActionComplete
    }
}
