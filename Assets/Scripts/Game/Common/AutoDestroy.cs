﻿using UnityEngine;

namespace Game.Common
{
    public class AutoDestroy : MonoBehaviour
    {
        [SerializeField]
        private float destroyAfter = 1f;
        
        private Animator animator;

        [SerializeField]
        private GameObject cristal;

        private void Awake()
        {
            animator = GetComponent<Animator>();
        }

        private void OnTriggerEnter(Collider other)
        {
            Invoke(nameof(AnimateDestroy), destroyAfter);
        }

        private void AnimateDestroy()
        {
            animator.SetInteger("State", 1);
        }

        public void DoDestroy()
        {
            MMGameEvent.Trigger(GameEvents.StateComplete);
            gameObject.SetActive(false);
            animator.SetInteger("State", 0);
            if (cristal) cristal.SetActive(false);
        }
    }
}
